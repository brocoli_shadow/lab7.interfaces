package main;

import lib.polymorphism.*;

public class IterableDemo {

	public static void main(String[] args) {

		MultipleDice md = new MultipleDice(5);

		System.out.println("\n Printing with for-each loop");
		for (Die die : md) {
			System.out.println(die.toString());

		}

		System.out.println("\n Printing with forEach loop");
		md.forEach(n -> System.out.println(n.toString()));

	}

}
